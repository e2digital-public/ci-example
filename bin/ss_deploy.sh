#!/bin/bash

TOKEN=${PLATFORM_DEPLOY_KEY}
PLATFORM="https://platform.silverstripe.com/naut"
ACCEPT=(-H "Accept: application/vnd.api+json")
PROJECT=$1
ENVIRO=$2
COMMIT_REF=$3
DEPLOY_FILE=$(mktemp)

echo -n "${PROJECT}: Triggering Git pull: "
# Trigger a Git Pull
PULL=$(curl -sS -X POST -u ${TOKEN} "${ACCEPT[@]}" ${PLATFORM}/project/${PROJECT}/git/fetches | jq -r '.data.links.self')

# wait for pull to finish
( while [ x${STATE} != "xComplete" ]; do  STATE=$(curl -sS -X GET -u ${TOKEN} "${ACCEPT[@]}" ${PULL} | jq -r '.data.attributes.status'); sleep 5; done ) && echo "Complete"

cat <<EOF >${DEPLOY_FILE}
{
    "ref": "${COMMIT_REF}",
    "ref_type": "sha",
    "title": "Automatic Deploy",
    "summary": "${PROJECT} - ${ENVIRO} - ${COMMIT_REF}",
    "bypass_and_start": true
}
EOF

echo -n "${PROJECT}: Creating new deploy for commit: ${COMMIT_REF}: " 
# Trigger a deploy
DEPLOY=$(curl -sS -X POST -u ${TOKEN} "${ACCEPT[@]}" -H "Content-Type: application/json" -d @${DEPLOY_FILE} ${PLATFORM}/project/${PROJECT}/environment/${ENVIRO}/deploys | jq -r '.data.links.self')

# Check if deploy is happy
( while [ x${STATE} != "xCompleted" ]; do STATE=$(curl -sS -X GET -u ${TOKEN} "${ACCEPT[@]}" ${DEPLOY} | jq -r '.data.attributes.state'); sleep 5; done ) && echo "Complete"

rm ${DEPLOY_FILE}
